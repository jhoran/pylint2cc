"PyLint output for gitlab ci"
import json
import html
import sys
import hashlib
from pylint.interfaces import IReporter
from pylint.reporters import BaseReporter

class CodeClimateReporter(BaseReporter):
   """Report messages and layouts in CodeClimate format for gitlab ci."""
   __implements__ = IReporter
   name = "codeclimate"
   extension = "json"

   category_map = {
      'convention': ['Style'],
      'refactor': ['Complexity'],
      'warning': ['Bug Risk'],
      'error': ['Bug Risk'],
      'fatal': ['Bug Risk'],
   }
   severity_map = {
      'convention': 'info',
      'refactor': 'info',
      'warning': 'normal',
      'error': 'critical',
      'fatal': 'critical',
   }

   def __init__(self, output=sys.stdout):
      BaseReporter.__init__(self, output)
      self.messages = []

   def handle_message(self, msg):
      """Manage message of different type and in the context of path."""
      self.messages.append(
         {
            "type": "issue",
            "check_name": msg.symbol,
            "description": html.escape(msg.msg or "", quote=False),
            "fingerprint": (hashlib
               .md5(('%s;%d;%s' % (msg.module, msg.line, msg.msg_id)).encode())
               .hexdigest()),
            "location": {
               "path": msg.path,
               "lines": {
                  "begin": msg.line
               }
            },
            "severity": self.severity_map[msg.category],
            "categories": self.category_map[msg.category]
         }
      )

   def display_messages(self, layout):
      """Launch layouts display"""
      print(json.dumps(self.messages, indent=2), file=self.out)

   def display_reports(self, layout):
      """Don't do anything in this reporter."""

   def _display(self, layout):
      """Do nothing."""


def register(linter):
   """Register the reporter classes with the linter."""
   linter.register_reporter(CodeClimateReporter)
