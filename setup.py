from setuptools import setup

setup(
   name='pylint2cc',
   version='0.1',
   description='Pylint output for Gitlab CI',
   author='John Horan',
   author_email='knasher@gmail.com',
   packages=['pylint2cc'],
   package_dir={'pylint2cc': 'src'},
   install_requires=["pylint"],
)
