# PyLint2CC

Simple formatter to turn pylint output into something that gitlab understands.
The format matches what is mentioned in the [gitlab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html).

### Setup
Add the following to your `.gitlab-ci.yml` to enable linting
```yaml
Lint:
  stage: test
  before_script:
    - apk update
    - apk add build-base musl-dev
    - pip install git+https://gitlab.com/jhoran/pylint2cc.git#egg=pylint2cc
  script:
    - pylint -f pylint2cc.CodeClimateReporter
        --disable=import-error
        --disable=bad-indentation
        --disable=bad-continuation
      [src-dir] | tee quality.json
  allow_failure: true
  artifacts:
    reports:
      codequality: quality.json
```
